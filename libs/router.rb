require '../config/appfront'

class Router
  include Resque::Plugins::UniqueJob

  #la registrazione del box avviene alla prima chiamata alle api
  #fare in modo che l'autenticazione venga gestita con cognizione di causa

  #direi che ogni cluster deve avere una sola chiave, univoca, che
  #viene create dal pannello ad ogni aggiunta
  #viene usata per l'autenticazione

  @queue = :router

  def self.perform(options)
    op = options.delete 'ops'
    Router::send op, options
  end

  private
  def self.create(options)
    Resque::enqueue(self, options)
  end

  def self.refresh_routes options

    token = nil
    token = ENV['CLUSTER_TOKEN']
    raise 'parameters are required' unless token

    #requesting cluster routes
    url_to_call = "https://#{token}:x@api.appfront.io/v1/cluster/deploys/routes"
    response = (RestClient.get url_to_call, { accept: :json }).body
    routes = JSON::parse response

    redis = Redis.new(:host => '127.0.0.1', :port => 6379, :db => 0)

    #aggiunta nuove rotte
    routes.each do |route|
      domain = route['domain']
      address = route['route']

      check = redis.lrange "frontend:#{domain}", 0, -1
      if check == []
        redis.rpush "frontend:#{domain}", domain 
        redis.rpush "frontend:#{domain}", "http://#{address}"
      else
        #qui bisogna aggiungere quelle che non ci sono 
        live_routes = redis.lrange "frontend:#{domain}", 0, -1
        unless live_routes.include? "http://#{address}"
          redis.rpush "frontend:#{domain}", "http://#{address}"
        end
      end
    end

    #rimozione vecche rotte
    routes.each do |route|
      name = route['domain']
      live_routes = redis.lrange "frontend:#{name}", 0, -1
      live_routes.each do |live_route|
        next if live_route == "#{name}"
        found = false
        routes.each do |r|
          if r['domain'] == name
            found = true if "http://#{r['route']}" == live_route
          end
        end
        unless found
          redis.lrem "frontend:#{name}", 1, live_route 
        end
      end
    end

    domains = redis.keys('*')

    domains.each do |domain|
      name = domain.split(':')[1]
      found = false
      routes.each {|r| found = true if r['domain'] == name }
      redis.del domain unless found
    end

    true
  rescue => e
    FrontOps::error "Router error: #{e}: #{e.backtrace.inspect}"
    false
  end

end
