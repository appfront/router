require 'remote_syslog_logger'

class FrontOps
  def self.info(msg, ops = {})
    msg = "INFO: #{msg}"
    ops.merge! severity: 6
    send :log, msg, ops
  end

  def self.error(msg, ops = {})
    msg = "ERROR: #{msg}"
    ops.merge! severity: 2
    send :log, msg, ops
  end

  def self.debug(msg, ops = {})
    msg = "DEBUG: #{msg}"
    ops.merge! severity: 7
    send :log, msg, ops
  end

  def self.warn(msg, ops = {})
    msg = "WARN: #{msg}"
    ops.merge! severity: 4
    send :log, msg, ops
  end

  def self.log(msg, options = {})
    options[:severity] ||= 2
    options[:program]  ||= 'sinatra'
    options[:context]  ||= 'box'

    l = RemoteSyslogLogger.new('logs2.papertrailapp.com', 17691, program: options[:program])
    l.log options[:severity], "#{msg}", options[:context]

  end
end