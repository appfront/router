$LOAD_PATH.unshift File.dirname(__FILE__)

$: << '..'

require 'bundler/setup'
Bundler.require

require 'rest-client'
require 'redis'

require 'resque/tasks'
require 'resque/pool/tasks'

require 'resque'
require 'resque-loner'
require 'resque-status'
require 'resque-timeout'



require 'libs/frontops'
require 'libs/router'

Resque.redis           = '127.0.0.1:6379:1'
Resque.redis.namespace = 'appfront:ops'

Resque::Plugins::Timeout.timeout = 600
